# Usage Data

This module is intended for internal site analytics, treating things like page views, clicks and downloads as events
which are recorded to a table, possibly to replace Drupal core's Statistics module.

## Installation

1. Install module as usual.

2. Allow to execute custom controller in your .htaccess. Below the Statistics module's rule add similar condition:

   RewriteCond %{REQUEST_URI} !/modules/contrib/usage_data/usage_data\.php$

## Plugins

"Usage Type" Plugins control what is tracked and data structure. There is a default "Generic" plugin to use as an
example. Though by default only `hook_entity_view()` is triggering usage, you can use click-tracking, or custom code to
place your own versions for tracking of virtually any usage your want.

To count entity events as a total sum your plugin should implement `UsageTypeCountInterface.php`. Counter field will be 
added to the schema automatically. Please check `GenericCounter` Usage Type plugin example.

## Queues

Since this module can result in many inserts to the database, this can be problematic on heavy traffic sites. To remedy
this, each plugin supports the Queue API. Obviously the default DB queue won't help with lightening the load on the
database, see the Redis example below for an alternative.

### Redis queue

1. Make sure that you enabled and setup <a href="https://www.drupal.org/project/redis" target="_blank">Redis</a> module.

2. Add to your settings.php:

   ##### Use this to only use Redis for a specific queue (usage_data_records in this case).
   $settings['queue_service_usage_data_records'] = 'queue.redis';

## Click tracking

Your tracked link should have ```data-usage=""``` attribute with usage data as a JSON string. For example:

    <a href="https://example.com/search?arg1=1&arg2=2" data-usage="{'entity_id':'27','entity_type_id':'media'}">Tracked link</a>

#### Parameters:

    Required:
      - entity_type_id - entity type id.
      - entity_id - entity id.

    Optional:
      - path - if empty it will be set from the link href or current page url.

## Views integration

Each "Usage Type" plugin can be exposed as a views field to count usage event type (click, view or download) per entity.

To enable this integration make sure that your "Usage Type" plugin has defined constant with needed entity type ids:

    const ENTITY_TYPES = ['node', 'taxonomy_term', 'media', 'commerce_product'];  

##

**Authors (V2):** Bryan Sharpe (b_sharpe) & Vladimir Selivanov (vselivanov)

**Original Authors:** Nia Kathoni (nikathone) & Daniel Cothran (andileco)
