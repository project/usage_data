<?php

/**
 * @file
 * Handles counts of entity views via AJAX with minimal bootstrap.
 */

use Drupal\Core\DrupalKernel;
use Drupal\usage_data\UsageDataInterface;
use Symfony\Component\HttpFoundation\Request;

// Work when this module is at web/modules/contrib/<module_name>.
chdir('../../..');
$autoloader = require_once 'autoload.php';
$kernel = DrupalKernel::createFromRequest(Request::createFromGlobals(), $autoloader, 'prod');
$kernel->boot();
$container = $kernel->getContainer();

$request = $container->get('request_stack');
$request->push(Request::createFromGlobals());
$usageDataService = $container->get('usage_data.usage');
$post = $request->getCurrentRequest()->request->all();

// Parse post data.
if ($request && is_array($post)) {
  // Check click event.
  if (isset($post['type']) && $post['type'] == UsageDataInterface::EVENT_TYPE_CLICK && !empty($post['info'])) {
    $entityTypeId = isset($post['info']['entity_type_id']) ? $post['info']['entity_type_id'] : NULL;
    $entityId = isset($post['info']['entity_id']) ? $post['info']['entity_id'] : NULL;
    $render = [];
    $data = $usageDataService->getUsageData(UsageDataInterface::EVENT_TYPE_CLICK, $entityTypeId, $entityId, $render, $post['info']);
    if (empty($data)) {
      return;
    }
    $post = [$data];
  }

  // For performance, we'll be doing multi-row inserts here.
  $inserts = [];
  foreach ($post as $data) {
    $usageDataService->prepareUsageData($data, $inserts);
  }

  // Record all applicable data.
  if (isset($inserts['db']) && !empty($inserts['db'])) {
    $container->get('usage_data.storage.database')->recordUsage($inserts['db']);
  }
  if (isset($inserts['queue']) && !empty($inserts['queue'])) {
    $queue = $container->get('queue')->get('usage_data_records');
    $queue->createItem($inserts['queue']);
  }
}
