<?php

namespace Drupal\usage_data;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Url;
use Drupal\usage_data\Event\CollectExtraDataEvent;
use Drupal\usage_data\Event\RecordingViewEvent;
use Drupal\usage_data\Event\UsageDataEvents;
use Drupal\usage_data\Plugin\UsageTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The usage data service.
 *
 * A utility service to assist with tracking usage data.
 */
class UsageData implements UsageDataInterface {

  /**
   * Usage type manager.
   *
   * @var \Drupal\usage_data\Plugin\UsageTypeManagerInterface
   */
  protected UsageTypeManagerInterface $usageTypeManager;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $request;

  /**
   * Constructs a new usage data object.
   *
   * @param \Drupal\usage_data\Plugin\UsageTypeManagerInterface $usageTypeManager
   *   The usage type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Used for dispatching social auth events.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(UsageTypeManagerInterface $usageTypeManager, EventDispatcherInterface $eventDispatcher, ConfigFactoryInterface $configFactory, ModuleHandlerInterface $moduleHandler, RequestStack $requestStack) {
    $this->usageTypeManager = $usageTypeManager;
    $this->eventDispatcher = $eventDispatcher;
    $this->configFactory = $configFactory;
    $this->moduleHandler = $moduleHandler;
    $this->request = $requestStack->getCurrentRequest();
  }

  /**
   * {@inheritDoc}
   */
  public function getUsageData($eventType, $entityTypeId, $entityId, array &$render = [], array $additionalData = []) {
    $data = [];
    $activePluginDefinitions = $this->usageTypeManager->getActiveDefinitions();
    foreach ($activePluginDefinitions as $id => $definition) {
      /** @var \Drupal\usage_data\Plugin\UsageTypeInterface $plugin */
      $plugin = $this->usageTypeManager->createInstance($id);
      if ($parsed = $plugin->parseEvent($eventType, $entityTypeId, $entityId, $render, $additionalData)) {
        // Collect extra data event.
        $event = new CollectExtraDataEvent($parsed);
        $this->eventDispatcher->dispatch($event, UsageDataEvents::COLLECT_EXTRA_DATA);
        $parsed = $event->getExtraData();

        $data[$id] = $parsed;
      }
    }
    return $data;
  }

  /**
   * {@inheritDoc}
   */
  public function prepareUsageData(array $data, array &$inserts) {
    $request_time = $this->request->server->get('REQUEST_TIME');
    $config = $this->configFactory->get('usage_data.settings')->getRawData();
    $activePluginDefinitions = $this->usageTypeManager->getActiveDefinitions();
    foreach ($activePluginDefinitions as $id => $definition) {
      if (isset($data[$id])) {
        $usage = $data[$id];
        $class = $definition['class'];
        $class::validateEvent($usage);
        if (!empty($usage['skip'])) {
          // @todo maybe log this?
          continue;
        }

        // Timestamp is same for all.
        $usage['timestamp'] = $request_time;

        // Right before we dispatch our event let decode the json into an array
        // so that other module can retrieve their data and clean it up.
        // @todo not sure this is really needed now that plugins control data.
        if (!empty($usage['extra_data'])) {
          $usage['extra_data'] = json_decode($usage['extra_data'], TRUE);
        }

        // This allows other modules to extract the extra data for example and
        // assign it to the proper column.
        $event = new RecordingViewEvent($usage);
        $this->eventDispatcher->dispatch($event, UsageDataEvents::RECORD_VIEW);
        $usage = $event->getData();

        // Removing extra data.
        if (isset($usage['extra_data'])) {
          unset($usage['extra_data']);
        }

        // Prepare for Redis Queue or DB.
        $key = $usage['event_type'] . $usage['entity_type_id'] . $usage['entity_id'];
        if (!empty($config[$id]['use_queue'])) {
          $inserts['queue'][$id][$key] = $usage;
        }
        else {
          $inserts['db'][$id][$key] = $usage;
        }
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getPostUrl() {
    $path = \Drupal::service('extension.list.module')->getPath('usage_data');
    return Url::fromUri('base:' . $path . '/usage_data.php')->toString();
  }

}
