<?php

namespace Drupal\usage_data\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\usage_data\Plugin\UsageTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Usage Data settings form management.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Usage type manager.
   *
   * @var \Drupal\usage_data\Plugin\UsageTypeManagerInterface
   */
  protected UsageTypeManagerInterface $usageTypeManager;

  /**
   * Plugin definitions.
   *
   * @var array
   */
  protected array $definitions;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Creates a SettingsForm instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory class instance.
   * @param \Drupal\usage_data\Plugin\UsageTypeManagerInterface $usageTypeManager
   *   The usage type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(ConfigFactoryInterface $config_factory, UsageTypeManagerInterface $usageTypeManager, ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory);
    $this->usageTypeManager = $usageTypeManager;
    $this->moduleHandler = $module_handler;
    $this->definitions = $this->usageTypeManager->getDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.usage_type'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'usage_data_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $settings = $this->config('usage_data.settings');

    $roles = array_map('\Drupal\Component\Utility\Html::escape', user_role_names());
    $form['exclude_roles'] = [
      '#title' => $this->t('Exclude Roles from tracking'),
      '#description' => $this->t('Select roles to exclude them from adding usage'),
      '#type' => 'checkboxes',
      '#options' => $roles,
      '#default_value' => $settings->get('exclude_roles') ?? [],
    ];

    $default_bots = 'bot|spider|crawl|googlebot|baidu|bing|msn|teoma|slurp|yandex|duckduck|semrush|yahoo';
    $form['bot_pattern'] = [
      '#title' => $this->t('A JS regex pattern to detect bots.'),
      '#description' => $this->t('The user agent will be tested against this regex (with /i) and will not track.'),
      '#type' => 'textfield',
      '#default_value' => $settings->get('bot_pattern') ?? $default_bots,
    ];

    foreach ($this->definitions as $definition) {
      $id = $definition['id'];
      $config = $settings->get($id);
      $form[$id] = [
        '#type' => 'details',
        '#title' => $definition['label'],
        '#collapsible' => TRUE,
        '#open' => TRUE,
      ];
      $form[$id][$id . '_enabled'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable plugin'),
        '#default_value' => isset($config['enabled']) ? $config['enabled'] : FALSE,
      ];
      $form[$id][$id . '_use_queue'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable Queue'),
        '#description' => $this->t('Use queue to process items'),
        '#default_value' => !empty($config['use_queue']) ?? FALSE,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('usage_data.settings');

    foreach ($this->definitions as $definition) {
      $id = $definition['id'];
      $values = $config->get($id);
      $values['enabled'] = $form_state->getValue($id . '_enabled');
      $values['use_queue'] = $form_state->getValue($id . '_use_queue');
      $config->set($id, $values);
    }

    $excluded_roles = array_keys(array_filter($form_state->getValue('exclude_roles')));
    $config->set('exclude_roles', $excluded_roles);

    $config->set('bot_pattern', $form_state->getValue('bot_pattern'));

    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['usage_data.settings'];
  }

}
