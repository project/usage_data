<?php

namespace Drupal\usage_data;

/**
 * Usage Data storage interface.
 */
interface UsageDataStorageInterface {

  /**
   * Records Usage keyed by type.
   *
   * @param array $data
   *   The data to insert.
   */
  public function recordUsage(array $data);

  /**
   * Record typed usage.
   *
   * Inserts are done in a single transaction for performance reasons.
   *
   * @param string $type
   *   The type of usage to store.
   * @param array $data
   *   The data to insert.
   * @param bool $isCounter
   *   Plugin is counter: increment record.
   *
   * @return bool
   *   TRUE if the usage has been stored.
   */
  public function recordUsageByType($type, array $data, $isCounter = FALSE);

  /**
   * Fetches usage data query for the appropriate table.
   *
   * NOTE: This function does NOT provide the fields() call as the entire
   * purpose is to allow altering of the query. Without adding this method
   * you will not be able to execute.
   *
   * @param string $type
   *   The type of usage data to fetch.
   * @param string $eventType
   *   The optional event type.
   * @param string $entityTypeId
   *   The optional entity type id.
   * @param string $entityId
   *   The optional entity id.
   *
   * @return \Drupal\Core\Database\Query\SelectInterface
   *   The query for the usage type.
   */
  public function fetchUsageQuery($type, $eventType = NULL, $entityTypeId = NULL, $entityId = NULL);

  /**
   * Fetches usage data from the appropriate table.
   *
   * @param string $type
   *   The type of usage data to fetch.
   * @param string $eventType
   *   The optional event type.
   * @param string $entityTypeId
   *   The optional entity type id.
   * @param string $entityId
   *   The optional entity id.
   * @param bool $count
   *   Whether this should be a count query or not.
   * @param array $conditions
   *   Additional conditions to add to the query.
   *
   * @return \Drupal\Core\Database\StatementInterface|mixed
   *   Either the executed query or the count.
   */
  public function fetchUsageData($type, $eventType = NULL, $entityTypeId = NULL, $entityId = NULL, $count = TRUE, array $conditions = []);

  /**
   * Delete usage data from all tables.
   *
   * @param int $entityTypeId
   *   The target entity type id.
   * @param int $entityId
   *   The target entity id.
   */
  public function deleteUsageData($entityTypeId, $entityId);

  /**
   * Defines the default schema for usage plugins.
   *
   * @return array
   *   The default schema.
   */
  public function defaultSchema();

  /**
   * Creates a table by type.
   *
   * @param string $type
   *   The type of usage.
   */
  public function createTable($type);

  /**
   * Drops a table by type.
   *
   * @param string $type
   *   The type of usage.
   */
  public function dropTable($type);

}
