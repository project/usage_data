<?php

namespace Drupal\usage_data\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\usage_data\UsageDataInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for notification_provider plugins.
 */
abstract class UsageTypePluginBase extends PluginBase implements UsageTypeInterface, ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected CurrentPathStack $currentPath;

  /**
   * Constructs a new plugin base.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPath
   *   The current path service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, CurrentPathStack $currentPath) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->currentPath = $currentPath;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('path.current')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return (string) $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public static function eventTypes() {
    return [
      UsageDataInterface::EVENT_TYPE_VIEW => UsageDataInterface::EVENT_TYPE_VIEW,
      UsageDataInterface::EVENT_TYPE_CLICK => UsageDataInterface::EVENT_TYPE_CLICK,
      UsageDataInterface::EVENT_TYPE_DOWNLOAD => UsageDataInterface::EVENT_TYPE_DOWNLOAD,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function schema() {
    return [];
  }

  /**
   * {@inheritdoc}
   *
   * @todo does this possibly need more to prevent corrupt posts?
   */
  public static function validateEvent(array &$data) {
    // Alter event types with hook_usage_data_event_types().
    $eventTypes = self::eventTypes();
    // @todo add static plugin id to the hook.
    \Drupal::moduleHandler()->invokeAll('usage_data_event_types',
      [&$eventTypes, $data]);
    if (!in_array($data['event_type'], $eventTypes)) {
      $data['skip'] = TRUE;
    }

    $data['entity_type_id'] = filter_var($data['entity_type_id'], FILTER_UNSAFE_RAW);
    $data['entity_id'] = $data['entity_id'] ? filter_var($data['entity_id'], FILTER_UNSAFE_RAW) : NULL;
  }

  /**
   * {@inheritdoc}
   *
   * @todo this would need to be static if used in storage, maybe make annotated?
   */
  public function tableSuffix() {
    return $this->id();
  }

  /**
   * Constructs the usual defaults for data usage.
   *
   * @param string $eventType
   *   The event type.
   * @param string $entityTypeId
   *   The entity type id.
   * @param string $entityId
   *   The entity id.
   *
   * @return array
   *   The default data.
   */
  protected function defaultData($eventType, $entityTypeId, $entityId) {
    return [
      'event_type' => $eventType,
      'entity_type_id' => $entityTypeId,
      'entity_id' => $entityId,
    ];
  }

  /**
   * Validates other types (route, views).
   *
   * @param string $entityTypeId
   *   The entity type id.
   *
   * @return bool
   *   True if valid, false otherwise.
   */
  public static function isValidOtherType($entityTypeId) {
    foreach (UsageDataInterface::OTHER_ENTITY_TYPES as $type) {
      if (strpos($entityTypeId, $type) === 0) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
