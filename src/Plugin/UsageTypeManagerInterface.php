<?php

namespace Drupal\usage_data\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Provides an interface for the UsageTypeManager.
 */
interface UsageTypeManagerInterface extends PluginManagerInterface {

  /**
   * Gets the definition of all enabled plugins for this type.
   *
   * @return mixed[]
   *   An array of plugin definitions (empty array if no definitions were
   *   found). Keys are plugin IDs.
   *
   * @see \Drupal\Core\Plugin\FilteredPluginManagerInterface::getFilteredDefinitions()
   */
  public function getActiveDefinitions();

  /**
   * Gets available event types.
   *
   * @return array
   *   An array of available event types for active plugins.
   */
  public function getAvailableEventTypes();

  /**
   * Check if Usage Type plugin is a simple counter.
   *
   * @param string $type
   *   Usage Type plugin machine name.
   *
   * @return mixed
   *   If Usage Type plugin counter or
   *   null if it's disabled or doesn't exists.
   */
  public function isCounterUsageType($type);

  /**
   * Gets the counter field for the Usage Type plugin.
   *
   * @param string $type
   *   Usage Type plugin machine name.
   * @return string
   *   The counter field name or an empty string.
   */
  public function getCounterUsageTypeFieldName(string $type): string;

}
