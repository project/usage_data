<?php

namespace Drupal\usage_data\Plugin;

/**
 * Interface for usage_type plugins.
 */
interface UsageTypeInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Get the id of the usage type.
   *
   * @return string
   *   The plugin id.
   */
  public function id();

  /**
   * Get a list of event types that this provider creates.
   *
   * @return array
   *   The event types.
   */
  public static function eventTypes();

  /**
   * Provides additional schema requirements.
   *
   * @return array
   *   Additions to the default schema.
   */
  public static function schema();

  /**
   * Validate and optionally skip data usage.
   *
   * @param array $data
   *   The data to parse.
   */
  public static function validateEvent(array &$data);

  /**
   * Populate data to be inserted.
   *
   * @param string $eventType
   *   The event type.
   * @param string $entityTypeId
   *   The entity type id.
   * @param mixed $entityId
   *   The entity id.
   * @param array $render
   *   The render array of the element.
   * @param array $additionalData
   *   The array of additional data for extra columns.
   *
   * @return array
   *   The populated usage data.
   */
  public function parseEvent($eventType, $entityTypeId, $entityId, array &$render = [], array $additionalData = []);

  /**
   * Provides the string to append to the table name.
   *
   * @return string
   *   The suffix of the table to store usage.
   */
  public function tableSuffix();

}
