<?php

namespace Drupal\usage_data\Plugin\UsageType;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\usage_data\Plugin\UsageTypePluginBase;
use Drupal\usage_data\UsageDataInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the Legacy UsageType.
 *
 * @UsageType(
 *   id = "legacy",
 *   label = @Translation("Supports Usage Data from V1."),
 *   description = @Translation("Tracks all events of all types with additional
 *    user and role data.")
 * )
 */
class Legacy extends UsageTypePluginBase {

  /**
   * The most common front-end viewable entities.
   */
  const ENTITY_TYPES = ['node', 'taxonomy_term', 'media', 'commerce_product', 'group'];

  /**
   * Only track full/default view modes.
   */
  const ENTITY_VIEW_MODES = ['default', 'full'];

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, CurrentPathStack $currentPath, AccountProxyInterface $currentUser) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entityTypeManager, $currentPath);
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('path.current'),
      $container->get('current_user')
    );
  }

  /**
   * Adds user information to the base tables.
   */
  public static function schema() {
    return [
      'fields' => [
        // Keeping entity_id as string to really match V1.
        'entity_id' => [
          'description' => 'The entity id for these statistics.',
          'type' => 'varchar_ascii',
          'not null' => TRUE,
          'default' => '',
          'length' => '255',
          'views_field_label' => 'Entity ID',
          'views_field_type' => 'standard',
          'views_sort_type' => 'standard',
          'views_filter_type' => 'string',
          'views_argument_type' => 'standard',
        ],
        'path' => [
          'type' => 'text',
          'not null' => FALSE,
          'description' => 'Path of the event.',
          'views_field_label' => t('Path'),
          'views_field_type' => 'standard',
          'views_sort_type' => 'standard',
          'views_filter_type' => 'string',
          'views_argument_type' => 'standard',
        ],
        'uid' => [
          'description' => 'The user ID.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
          'views_field_label' => t('UID'),
          'views_field_type' => 'standard',
          'views_sort_type' => 'standard',
          'views_filter_type' => 'user_name',
          'views_argument_type' => 'user_uid',
        ],
        'user_name' => [
          'description' => 'The username of the user.',
          'type' => 'varchar_ascii',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'views_field_label' => t('Username'),
          'views_field_type' => 'standard',
          'views_sort_type' => 'standard',
          'views_filter_type' => 'string',
          'views_argument_type' => 'standard',
        ],
        'user_role' => [
          'description' => 'The role of the user.',
          'type' => 'varchar_ascii',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'views_field_label' => t('Role'),
          'views_field_type' => 'standard',
          'views_sort_type' => 'standard',
          'views_filter_type' => 'user_roles',
          'views_argument_type' => 'user__roles_rid',
        ],
        'count' => [
          'description' => 'Simple count for each event.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 1,
          'size' => 'small',
          'views_field_label' => t('Simple count for each event.'),
          'views_field_type' => 'numeric',
          'views_sort_type' => 'numeric',
          'views_filter_type' => 'numeric',
          'views_argument_type' => 'numeric',
        ],
      ],
      'indexes' => [
        'uid' => ['uid'],
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function parseEvent($eventType, $entityTypeId, $entityId, array &$render = [], array $additionalData = []) {
    $viewMode = isset($additionalData['view_mode']) ? $additionalData['view_mode'] : FALSE;
    if ((!str_starts_with($entityTypeId, 'view:') && !str_starts_with($entityTypeId, 'route:page_manager.')) && !in_array($viewMode, self::ENTITY_VIEW_MODES)) {
      return FALSE;
    }

    // In v1 entity_type_id is the concatenation of view and the display id.
    if (str_starts_with($entityTypeId, 'view:') && !$entityId) {
      $entityId = str_replace('view:', '', $entityTypeId);
      $entityTypeId = 'view';
    }
    // Supports for page manager variant.
    if (!$entityId && str_starts_with($entityTypeId, 'route:') && \Drupal::moduleHandler()->moduleExists('page_manager')) {
      $current_route = \Drupal::routeMatch()->getRouteObject();
      if ($current_route && ($variant_id = $current_route->getDefault('page_manager_page_variant'))) {
        $entityId = $current_route->getDefault('page_manager_page') . ':' . $variant_id;
        $entityTypeId = 'page';
      }
    }

    if (!$entityId) {
      return FALSE;
    }

    $data = $this->defaultData($eventType, $entityTypeId, $entityId);

    $data['count'] = 1;

    $data['path'] = !empty($additionalData['path']) ? $additionalData['path'] : $this->currentPath->getPath();

    // V1 didn't use file entity here.
    if ($eventType == UsageDataInterface::EVENT_TYPE_DOWNLOAD) {
      $data['entity_type_id'] = 'file_download';
    }

    // Add user data.
    $roles = $this->currentUser->getRoles(TRUE);
    $data['uid'] = $this->currentUser->id();
    $data['user_name'] = $this->currentUser->getAccountName();
    $data['user_role'] = $roles ? implode(',', $roles) : '';

    // Because we're tracking users, we need to alter the render array to cache
    // per user context.
    if (!empty($render)) {
      $render['#cache']['context'][] = 'user';
    }

    // V1 considered media as a download.
    if ($entityTypeId === 'media') {
      $data['event_type'] = UsageDataInterface::EVENT_TYPE_DOWNLOAD;
    }
    return $data;
  }

  /**
   * {@inheritDoc}
   */
  public static function validateEvent(&$data) {
    parent::validateEvent($data);

    $data['path'] = filter_var($data['path'], FILTER_SANITIZE_URL);

    // If empty then every role can be tracked for view.
    $data['uid'] = (int) $data['uid'];

    // Regex validating alphanumeric,
    // underscore and commas which can be present if separating multiple roles.
    $current_user_roles = filter_var(
      $data['user_role'],
      FILTER_VALIDATE_REGEXP,
      ['options' => ['regexp' => '/^[A-Za-z0-9_,]+$/']]
    );
    $data['user_role'] = $current_user_roles;
    $data['user_name'] = filter_var($data['user_name'], FILTER_UNSAFE_RAW);
  }

}
