<?php

namespace Drupal\usage_data\Plugin\UsageType;

use Drupal\usage_data\Plugin\UsageTypeCountInterface;
use Drupal\usage_data\Plugin\UsageTypePluginBase;
use Drupal\usage_data\UsageDataInterface;

/**
 * Plugin implementation of the Generic Counter UsageType.
 *
 * @UsageType(
 *   id = "generic_counter",
 *   label = @Translation("Generic Usage Data Counter"),
 *   description = @Translation("Tracks total counts for all events of all types.")
 * )
 */
class GenericCounter extends UsageTypePluginBase implements UsageTypeCountInterface {

  /**
   * The most common front-end viewable entities.
   */
  const ENTITY_TYPES = ['node', 'taxonomy_term', 'media', 'commerce_product'];

  /**
   * Only track full/default view modes.
   */
  const ENTITY_VIEW_MODES = ['default', 'full'];

  /**
   * {@inheritDoc}
   */
  public function parseEvent($eventType, $entityTypeId, $entityId, array &$render = [], array $additionalData = []) {
    $viewMode = isset($additionalData['view_mode']) ? $additionalData['view_mode'] : FALSE;
    if ((in_array($entityTypeId, self::ENTITY_TYPES) && in_array($viewMode, self::ENTITY_VIEW_MODES)) ||
      parent::isValidOtherType($entityTypeId) ||
      $eventType == UsageDataInterface::EVENT_TYPE_CLICK) {
      return $this->defaultData($eventType, $entityTypeId, $entityId);
    }
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public static function validateEvent(array &$data) {
    $allowed_Types = self::ENTITY_TYPES;
    if (!parent::isValidOtherType($data['entity_type_id']) && !in_array($data['entity_type_id'], $allowed_Types)) {
      $data['skip'] = TRUE;
    }
    parent::validateEvent($data);
  }

}
