<?php

namespace Drupal\usage_data\Plugin\UsageType;

use Drupal\usage_data\Plugin\UsageTypePluginBase;
use Drupal\usage_data\UsageDataInterface;

/**
 * Plugin implementation of the Generic UsageType.
 *
 * @UsageType(
 *   id = "generic",
 *   label = @Translation("Generic Usage Data"),
 *   description = @Translation("Tracks all events of all types.")
 * )
 */
class Generic extends UsageTypePluginBase {

  /**
   * The most common front-end viewable entities.
   */
  const ENTITY_TYPES = ['node', 'taxonomy_term', 'media', 'commerce_product'];

  /**
   * Only track full/default view modes.
   */
  const ENTITY_VIEW_MODES = ['default', 'full'];

  /**
   * {@inheritDoc}
   */
  public function parseEvent($eventType, $entityTypeId, $entityId, array &$render = [], array $additionalData = []) {
    $viewMode = isset($additionalData['view_mode']) ? $additionalData['view_mode'] : FALSE;
    if ((in_array($entityTypeId, self::ENTITY_TYPES) && in_array($viewMode, self::ENTITY_VIEW_MODES)) ||
      parent::isValidOtherType($entityTypeId) ||
      $eventType == UsageDataInterface::EVENT_TYPE_CLICK) {
      $data = $this->defaultData($eventType, $entityTypeId, $entityId);

      $data['path'] = !empty($additionalData['path']) ? $additionalData['path'] : $this->currentPath->getPath();

      return $data;
    }
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public static function validateEvent(array &$data) {
    $allowed_Types = self::ENTITY_TYPES;
    if (!parent::isValidOtherType($data['entity_type_id']) && !in_array($data['entity_type_id'], $allowed_Types)) {
      $data['skip'] = TRUE;
    }

    $data['path'] = filter_var($data['path'], FILTER_SANITIZE_URL);
    parent::validateEvent($data);
  }

  /**
   * Adds an event path.
   */
  public static function schema() {
    return [
      'fields' => [
        'path' => [
          'type' => 'text',
          'not null' => FALSE,
          'description' => 'Path of the event.',
          'views_field_label' => t('Path'),
          'views_field_type' => 'standard',
          'views_sort_type' => 'standard',
          'views_filter_type' => 'string',
          'views_argument_type' => 'standard',
        ],
      ],
    ];
  }

}
