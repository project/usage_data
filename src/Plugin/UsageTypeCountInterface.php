<?php

namespace Drupal\usage_data\Plugin;

/**
 * Interface for usage_type plugins with incremental counter.
 *
 * Counter field will be added to the Usage Type Counter plugin schema.
 *
 * @see usageTypeSchema($type) method in the UsageDataDatabaseStorage.
 */
interface UsageTypeCountInterface {

  /**
   * Counter field name to be added to the schema.
   */
  const COUNTER_FIELD_NAME = 'counter';

}
