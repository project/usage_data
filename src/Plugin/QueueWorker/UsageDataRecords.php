<?php

namespace Drupal\usage_data\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Save Usage Data records to the database.
 *
 * @QueueWorker(
 *   id = "usage_data_records",
 *   title = @Translation("Usage Data records process"),
 *   cron = {"time" = 90}
 * )
 */
class UsageDataRecords extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    /** @var \Drupal\usage_data\UsageDataDatabaseStorage $databaseStorage */
    $databaseStorage = \Drupal::service('usage_data.storage.database');
    $databaseStorage->recordUsage($data);
  }

}
