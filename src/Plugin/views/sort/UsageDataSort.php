<?php

namespace Drupal\usage_data\Plugin\views\sort;

use Drupal\usage_data\Plugin\UsageTypeCountInterface;
use Drupal\views\Plugin\views\sort\SortPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handle sorting by usage.
 *
 * @ViewsSort("usage_data_sort")
 */
class UsageDataSort extends SortPluginBase {

  /**
   * The usage storage.
   *
   * @var \Drupal\usage_data\UsageDataStorageInterface
   */
  protected $usageStorage;

  /**
   * The views handler join.
   *
   * @var \Drupal\views\Plugin\ViewsHandlerManager
   */
  protected $viewsJoin;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->usageStorage = $container->get('usage_data.storage.database');
    $instance->viewsJoin = $container->get('plugin.manager.views.join');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $definition = $this->definition;
    $bases = $this->query->getEntityTableInfo();
    $base = $bases[$definition['entity_type']] ?? FALSE;

    // Make sure we can join.
    if ($base) {
      $queue = $this->query->getTableQueue();

      // Exposed filters get run twice, so we need to only join once.
      // @todo this could be fixed with proper views_data().
      if (empty($queue['usage_data'])) {
        if (!empty($definition['usage_plugin_is_counter'])) {
          $joinDefinition = [
            'table' => $definition['usage_plugin_table'],
            'field' => 'entity_id',
            'left_table' => $base['base'],
            'left_field' => $definition['entity_key'],
            'extra' => [
              ['field' => 'event_type', 'value' => $definition['usage_event_type'], 'operator' => '='],
              ['field' => 'entity_type_id', 'value' => $definition['entity_type'], 'operator' => '='],
            ],
          ];
        }
        else {
          // Join on total count.
          $usage_query = $this->usageStorage->fetchUsageQuery(
            $definition['usage_plugin_id'],
            $definition['usage_event_type'],
            $definition['entity_type'],
          );
          $usage_query->fields('u', ['entity_id']);
          $usage_query->addExpression('count(u.id)', 'total');
          $usage_query->groupBy('u.entity_id');

          $joinDefinition = [
            'table formula' => $usage_query,
            'field' => 'entity_id',
            'left_table' => $base['base'],
            'left_field' => $definition['entity_key'],
          ];
        }

        // Join the table to the base.
        $join = $this->viewsJoin->createInstance('standard', $joinDefinition);
        $this->query->addRelationship('usage_data', $join, $definition['entity_type']);
      }

      // Sort.
      $sortField = $definition['usage_plugin_is_counter'] ? UsageTypeCountInterface::COUNTER_FIELD_NAME : 'total';
      $this->query->addOrderBy('usage_data', $sortField, $this->options['order']);
    }
  }

}
