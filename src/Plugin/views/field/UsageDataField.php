<?php

namespace Drupal\usage_data\Plugin\views\field;

use Drupal\usage_data\Plugin\UsageTypeCountInterface;
use Drupal\usage_data\UsageDataDatabaseStorage;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\ViewsHandlerManager;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A handler to show Usage Data counter fields for different event types.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("usage_data_field")
 */
class UsageDataField extends FieldPluginBase {

  /**
   * Usage Data database storage.
   *
   * @var \Drupal\usage_data\UsageDataDatabaseStorage
   */
  protected $usageDataDatabaseStorage;

  /**
   * The views handler join.
   *
   * @var \Drupal\views\Plugin\ViewsHandlerManager
   */
  protected $viewsJoin;

  /**
   * Plugin constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\usage_data\UsageDataDatabaseStorage $usageDataDatabaseStorage
   *   Usage Data database storage.
   * @param \Drupal\views\Plugin\ViewsHandlerManager $viewsJoin
   *   The views handler join.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, UsageDataDatabaseStorage $usageDataDatabaseStorage, ViewsHandlerManager $viewsJoin) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->usageDataDatabaseStorage = $usageDataDatabaseStorage;
    $this->viewsJoin = $viewsJoin;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('usage_data.storage.database'),
      $container->get('plugin.manager.views.join')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing. Sit back, relax, have a mojito.
  }

  /**
   * {@inheritdoc}
   */
  public function clickSort($order) {
    $definition = $this->definition;
    $bases = $this->query->getEntityTableInfo();
    $base = $bases[$definition['entity_type']] ?? FALSE;
    if (!$base) {
      return;
    }

    $queue = $this->query->getTableQueue();
    if (empty($queue['usage_data'])) {

      if (!empty($definition['usage_plugin_is_counter'])) {
        $joinDefinition = [
          'table' => $definition['usage_plugin_table'],
          'field' => 'entity_id',
          'left_table' => $base['base'],
          'left_field' => $definition['entity_key'],
          'extra' => [
            ['field' => 'event_type', 'value' => $definition['usage_event_type'], 'operator' => '='],
            ['field' => 'entity_type_id', 'value' => $definition['entity_type'], 'operator' => '='],
          ],
        ];
      }
      else {
        // Join on total count.
        $usage_query = $this->usageDataDatabaseStorage->fetchUsageQuery(
          $definition['usage_plugin_id'],
          $definition['usage_event_type'],
          $definition['entity_type'],
          );
        $usage_query->fields('u', ['entity_id']);
        $usage_query->addExpression('count(u.id)', 'total');
        $usage_query->groupBy('u.entity_id');

        $joinDefinition = [
          'table formula' => $usage_query,
          'field' => 'entity_id',
          'left_table' => $base['base'],
          'left_field' => $definition['entity_key'],
        ];
      }

      // Join the table to the base.
      $join = $this->viewsJoin->createInstance('standard', $joinDefinition);
      $this->query->addRelationship('usage_data', $join, $definition['entity_type']);
    }

    // Sort.
    $sortField = $definition['usage_plugin_is_counter'] ? UsageTypeCountInterface::COUNTER_FIELD_NAME : 'total';
    $this->query->addOrderBy('usage_data', $sortField, $order);
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $values, $field = NULL) {
    if (empty($entity = $values->_entity)) {
      return '';
    }

    $count = $this->usageDataDatabaseStorage->fetchUsageData(
      $this->definition['usage_plugin_id'],
      $this->definition['usage_event_type'],
      $entity->getEntityTypeId(),
      $entity->id()
    );

    return $count === FALSE ? '' : (int) $count;
  }

}
