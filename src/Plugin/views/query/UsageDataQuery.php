<?php

namespace Drupal\usage_data\Plugin\views\query;

use Drupal\views\Plugin\views\query\Sql;

/**
 * usage_data views query plugin in order to query the tracking to views.
 *
 * @ViewsQuery(
 *   id = "usage_data_query",
 *   title = @Translation("Usage Data Query"),
 * )
 */
class UsageDataQuery extends Sql {

}
