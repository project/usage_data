<?php

namespace Drupal\usage_data\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides an UsageType plugin manager.
 */
class UsageTypeManager extends DefaultPluginManager implements UsageTypeManagerInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Constructs a UsageTypeManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory) {
    parent::__construct(
      'Plugin/UsageType',
      $namespaces,
      $module_handler,
      'Drupal\usage_data\Plugin\UsageTypeInterface',
      'Drupal\usage_data\Annotation\UsageType'
    );
    $this->alterInfo('usage_type_info');
    $this->setCacheBackend($cache_backend, 'usage_type_plugins');

    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveDefinitions() {
    $usageDataConfigs = $this->configFactory->get('usage_data.settings')->getRawData();

    $active = [];
    foreach ($this->getDefinitions() as $key => $definition) {
      if (isset($usageDataConfigs[$key]['enabled']) && !empty($usageDataConfigs[$key]['enabled'])) {
        $active[$key] = $definition;
      }
    }

    return $active;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableEventTypes() {
    $eventTypes = [];
    foreach ($this->getActiveDefinitions() as $definition) {
      $eventTypes += $definition['class']::eventTypes();
    }
    return $eventTypes;
  }

  /**
   * {@inheritdoc}
   */
  public function isCounterUsageType($type) {
    if (!($class = $this->getPluginDefinitionClassByType($type))) {
      return NULL;
    }

    return defined("$class::COUNTER_FIELD_NAME") && !empty($class::COUNTER_FIELD_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public function getCounterUsageTypeFieldName(string $type): string {
    if (!$this->isCounterUsageType($type)) {
      return '';
    }
    return $this->getPluginDefinitionClassByType($type);
  }

  private function getPluginDefinitionClassByType($type) {
    $definitions = $this->getDefinitions();
    if (!isset($definitions[$type]) || !is_array($definitions[$type])) {
      return NULL;
    }

    return $definitions[$type]['class'] ?? NULL;
  }

}
