<?php

namespace Drupal\usage_data;

/**
 * Interface definition for the main service.
 */
interface UsageDataInterface {

  /**
   * Special cases to track page views.
   */
  const OTHER_ENTITY_TYPES = ['view:', 'route:'];

  /**
   * Default event type "view".
   */
  const EVENT_TYPE_VIEW = 'view';

  /**
   * Default event type "click".
   */
  const EVENT_TYPE_CLICK = 'click';

  /**
   * Default event type "download".
   */
  const EVENT_TYPE_DOWNLOAD = 'download';

  /**
   * Retrieve the fully populated event data.
   *
   * @param string $eventType
   *   The event type.
   * @param string $entityTypeId
   *   The entity type id.
   * @param mixed $entityId
   *   The entity id.
   * @param array $render
   *   The render array of the element.
   * @param array $additionalData
   *   The array of additional data for extra columns.
   *
   * @return array
   *   The populated usage data.
   */
  public function getUsageData($eventType, $entityTypeId, $entityId, array &$render = [], array $additionalData = []);

  /**
   * Prepare Usage Data.
   *
   * @param array $data
   *   Usage data arrays keyed by Usage Type plugin id.
   * @param array $inserts
   *   Prepared array to save Usage Data grouped by queue or db key.
   */
  public function prepareUsageData(array $data, array &$inserts);

  /**
   * Helper to retrieve post path.
   *
   * @return \Drupal\Core\GeneratedUrl|string
   *   The path to the tracking script.
   */
  public function getPostUrl();

}
