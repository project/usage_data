<?php

namespace Drupal\usage_data;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Database\Connection;
use Drupal\usage_data\Plugin\UsageTypeCountInterface;
use Drupal\usage_data\Plugin\UsageTypeManagerInterface;

/**
 * Usage data database storage.
 *
 * For performance, it has been attempted to not instantiate classes where
 * possible and instead use static methods.
 */
class UsageDataDatabaseStorage implements UsageDataStorageInterface {

  /**
   * Sets the base table name.
   */
  const TABLE_BASE = 'usage_data';

  /**
   * Support for v1.
   */
  const LEGACY_TYPE = 'legacy';

  /**
   * The database connection used.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * The usage type manager.
   *
   * @var \Drupal\usage_data\Plugin\UsageTypeManagerInterface
   */
  protected UsageTypeManagerInterface $usageTypeManager;

  /**
   * Constructs the database storage.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection for the node view storage.
   * @param \Drupal\usage_data\Plugin\UsageTypeManagerInterface $usageTypeManager
   *   The usage type manager.
   */
  public function __construct(Connection $connection, UsageTypeManagerInterface $usageTypeManager) {
    $this->connection = $connection;
    $this->usageTypeManager = $usageTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function recordUsage(array $data) {
    foreach ($data as $type => $values) {
      $isCounter = $this->usageTypeManager->isCounterUsageType($type);
      // @todo log on failure?
      $this->recordUsageByType($type, $values, $isCounter);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function recordUsageByType($type, array $data, $isCounter = FALSE) {
    try {
      $table = $this->tableName($type);
      $fieldNames = array_keys($data[array_key_first($data)]);
      if ($isCounter) {
        $database_schema = $this->connection->schema();
        $counterField = $this->usageTypeManager->getCounterUsageTypeFieldName($type);
        if (!$database_schema->fieldExists($table, $counterField)) {
          throw new \Exception('Usage Data table ' . $table . ' schema should have a counter ' . $counterField . ' column.');
        }
        foreach ($data as $usage) {
          $sQuery = $this->connection->select($table);
          $sQuery->condition('event_type', $usage['event_type']);
          $sQuery->condition('entity_type_id', $usage['entity_type_id']);
          if ($usage['entity_id']) {
            $sQuery->condition('entity_id', $usage['entity_id']);
          }
          $sQuery->addField($table, 'id');
          $sQuery->addField($table, $counterField);
          $res = $sQuery->execute()->fetchAssoc();
          if ($res) {
            $query = $this->connection->update($table);
            $query->condition('id', $res['id']);
            $query->fields([
              $counterField => $res[$counterField] + 1,
              'timestamp' => $usage['timestamp'],
            ]);
          }
          else {
            $query = $this->connection->insert($table);
            $f = $fieldNames;
            $f[] = $counterField;
            $v = array_values($usage);
            $v[] = 1;
            $query->fields($f);
            $query->values($v);
          }
          $query->execute();
        }
      }
      else {
        $query = $this->connection->insert($table)
          ->fields($fieldNames);
        foreach ($data as $usage) {
          // Only pass the values since the order of $fields matches the order
          // of the insert fields. This is a performance optimization to avoid
          // unnecessary loops within the method.
          $query->values(array_values($usage));
        }
      }
      return (bool) $query->execute();
    }
    catch (\Exception $e) {
      $database_schema = $this->connection->schema();
      if ($database_schema->tableExists($this->tableName($type))) {
        throw $e;
      }
      else {
        $this->createTable($type);
        $this->recordUsageByType($type, $data, $isCounter);
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchUsageQuery($type, $eventType = NULL, $entityTypeId = NULL, $entityId = NULL) {
    // Make sure the table exists.
    $table = $this->tableName($type);
    $query = $this->connection->select($table, 'u');

    if ($eventType) {
      $query->condition('event_type', $eventType);
    }
    if ($entityTypeId) {
      $query->condition('entity_type_id', $entityTypeId);
    }
    if ($entityId) {
      $query->condition('entity_id', $entityId);
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchUsageData($type, $eventType = NULL, $entityTypeId = NULL, $entityId = NULL, $count = TRUE, $conditions = []) {
    // Make sure the table exists.
    $table = $this->tableName($type);
    if ($this->connection->schema()->tableExists($table)) {
      $query = $this->fetchUsageQuery($type, $eventType, $entityTypeId, $entityId);

      // @todo this can be done better.
      foreach ($conditions as $condition) {
        if (!empty($condition['field']) && !empty($condition['value'])) {
          $operator = $condition['operator'] ?? '=';
          $query->condition($condition['field'], $condition['value'], $operator);
        }
      }

      // Usage Type Counter plugin.
      if ($counter_field_name = $this->usageTypeManager->getCounterUsageTypeFieldName($type)) {
        $query->addField('u', $counter_field_name);
        return $query->execute()->fetchField();
      }

      if (!$count) {
        // @todo this could be configurable, just grab all for now.
        $query->fields('u');
      }

      return $count ? $query->countQuery()->execute()->fetchField() : $query->execute();
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteUsageData($entityTypeId, $entityId) {
    $pluginDefinitions = $this->usageTypeManager->getDefinitions();
    foreach ($pluginDefinitions as $definition) {
      $tableName = $this->tableName($definition['id']);
      if ($this->connection->schema()->tableExists($tableName)) {
        $this->connection->delete($tableName)
          ->condition('entity_id', $entityId)
          ->condition('entity_type_id', $entityTypeId)
          ->execute();
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function createTable($type) {
    $table = $this->tableName($type);
    $schema = NestedArray::mergeDeep($this->defaultSchema(), $this->usageTypeSchema($type));
    if (!$this->connection->schema()->tableExists($this->tableName($type))) {
      $this->connection->schema()->createTable($table, $schema);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function dropTable($type) {
    if ($this->connection->schema()->tableExists($this->tableName($type))) {
      $this->connection->schema()->dropTable($this->tableName($type));
    }
  }

  /**
   * {@inheritDoc}
   */
  public function defaultSchema() {
    return [
      'description' => 'Access usage data.',
      'fields' => [
        'id' => [
          'description' => 'The identifier for the schema.',
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'views_field_label' => 'ID',
          'views_field_type' => 'numeric',
          'views_sort_type' => 'numeric',
          'views_filter_type' => 'numeric',
          'views_argument_type' => 'numeric',
        ],
        'event_type' => [
          'description' => 'The event type.',
          'type' => 'varchar',
          'length' => 32,
          'not null' => TRUE,
          'default' => '',
          'views_field_label' => 'Event Type',
          'views_field_type' => 'standard',
          'views_sort_type' => 'standard',
          'views_filter_type' => 'string',
          'views_argument_type' => 'standard',
        ],
        'entity_type_id' => [
          'description' => 'The entity type id.',
          'type' => 'varchar',
          'length' => 32,
          'not null' => TRUE,
          'default' => '',
          'views_field_label' => 'Entity Type ID',
          'views_field_type' => 'standard',
          'views_sort_type' => 'standard',
          'views_filter_type' => 'string',
          'views_argument_type' => 'standard',
        ],
        'entity_id' => [
          'description' => 'The entity id for these statistics.',
          'type' => 'int',
          'unsigned' => TRUE,
          'views_field_label' => 'Entity ID',
          'views_field_type' => 'numeric',
          'views_sort_type' => 'numeric',
          'views_filter_type' => 'numeric',
          'views_argument_type' => 'numeric',
        ],
        'timestamp' => [
          'description' => 'The most recent time the event took place.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
          'views_field_type' => 'date',
          'views_sort_type' => 'date',
          'views_filter_type' => 'date',
          'views_argument_type' => 'date',
          'views_field_label' => 'Timestamp',
        ],
      ],
      'primary key' => ['id'],
      'indexes' => [
        'entity_type_id' => ['entity_type_id'],
        'entity_id' => ['entity_id'],
      ],
    ];
  }

  /**
   * Loads the schema specific to the usage plugin.
   *
   * @param string $type
   *   The usage type.
   *
   * @return array
   *   The schema provided by the usage type.
   */
  protected function usageTypeSchema($type) {
    $definitions = $this->usageTypeManager->getDefinitions();
    if (!empty($definitions[$type])) {
      $plugin_class = $definitions[$type]['class'];
      $schema = $plugin_class::schema();
      // Usage Type Counter plugin counter field.
      if (defined("$plugin_class::COUNTER_FIELD_NAME")) {
        $schema['fields'][UsageTypeCountInterface::COUNTER_FIELD_NAME] = [
          'description' => 'Usage data event counter.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
          'views_field_label' => 'Counter',
          'views_field_type' => 'numeric',
          'views_sort_type' => 'numeric',
          'views_filter_type' => 'numeric',
          'views_argument_type' => 'numeric',
        ];
      }
      return $schema;
    }
    return [];
  }

  /**
   * Act on an exception when the table might not have been created.
   *
   * If the table does not yet exist, that's fine, but if the table exists and
   * something else caused the exception, then propagate it.
   *
   * @param string $type
   *   The usage plugin type.
   * @param \Exception $e
   *   The exception.
   *
   * @throws \Exception
   */
  protected function catchException($type, \Exception $e) {
    if ($this->connection->schema()->tableExists($this->tableName($type))) {
      throw $e;
    }
  }

  /**
   * Generates the table name from the entity type.
   *
   * @param string $type
   *   The usage plugin type.
   *
   * @return string
   *   The table name.
   */
  public function tableName($type) {
    // @todo use plugin table suffix here.
    return $type === self::LEGACY_TYPE ? self::TABLE_BASE : self::TABLE_BASE . '_' . trim(strtolower($type));
  }

}
