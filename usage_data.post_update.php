<?php

/**
 * @file
 * Usage data module post-update file.
 */

use Drupal\usage_data\UsageDataDatabaseStorage;

/**
 * Enable legacy plugin to support v1 to v2 upgrade.
 */
function usage_data_post_update_enable_legacy_usage_type_plugin_for_v1(&$sandbox) {
  /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
  $config_factory = \Drupal::service('config.factory');
  $config = $config_factory->getEditable('usage_data.settings');

  // If there is existing config this is probably not a v1 to v2 update.
  if ($config->getRawData()) {
    return (string) t('There is existing configurations for the usage module no update were performed');
  }

  // No usage_data database table from v1, no need to upgrade to v2 from v1.
  $database = \Drupal::database();
  if (!$database->schema()->tableExists(UsageDataDatabaseStorage::TABLE_BASE)) {
    return (string) t('The usage_data table from v1 does not exist. No update were performed.');
  }

  $legacy_plugin_id = UsageDataDatabaseStorage::LEGACY_TYPE;
  $legacy_plugin_config = [
    'enabled' => TRUE,
    'use_queue' => FALSE,
  ];
  $config->set($legacy_plugin_id, $legacy_plugin_config);
  $config->save();
  return (string) t('The legacy plugin have been enabled to support the existing usage_data table schema from v1.');
}
