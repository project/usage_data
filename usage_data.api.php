<?php

/**
 * @file
 * Hooks and API provided by the Usage Data module.
 */

use Drupal\usage_data\UsageDataInterface;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alters UsageType event types.
 *
 * @param array $event_types
 *   The array of available event types being modified.
 * @param array $data
 *   The UsageType data array.
 *
 * @ingroup usage_data_api
 */
function hook_usage_data_event_types(array &$event_types, array $data) {
  if (!empty($event_types[UsageDataInterface::EVENT_TYPE_DOWNLOAD])) {
    unset($event_types[UsageDataInterface::EVENT_TYPE_DOWNLOAD]);
  }
}

/**
 * @} End of "addtogroup hooks".
 */
