/**
 * @file
 * Usage Data functionality.
 */

(function ($, Drupal, drupalSettings, once) {

  'use strict';

  $(document).ready(function () {
    // Track view event.
    if (drupalSettings.usage_data.data) {
      const botPattern = drupalSettings.usage_data.bot_pattern || false;
      if (botPattern) {
        let re = new RegExp(botPattern, 'i');
        if (re.test(navigator.userAgent)) {
          return;
        }
      }

      $.ajax({
        type: 'POST',
        cache: false,
        url: drupalSettings.usage_data.url,
        data: drupalSettings.usage_data.data
      });
    }
  });

  Drupal.behaviors.usageData = {
    attach: function (context, settings) {
      // Track click event.
      once('usageData', '[data-usage]', context).forEach(function (el) {
        el.addEventListener('click', function () {
          var data = JSON.parse($(this).attr('data-usage'));
          if (!data) {
            return;
          }
          // Add path info if not set.
          if (!data['path']) {
            var href = $(this).attr('href') ? $(this).attr('href') : location.href;
            data['path'] = href.replace(location.origin, '');
          }
          $.ajax({
            type: 'POST',
            cache: false,
            url: drupalSettings.usage_data.url,
            data: {
              'type': 'click',
              'info': data
            }
          });
        });
      });
    },
  };

  // Track reload for Ajax enabled views.
  $(document).ajaxComplete(function (event, xhr, settings) {
    if (xhr.responseJSON && typeof(xhr.responseJSON) === 'object' && xhr.responseJSON.length > 1) {
      xhr.responseJSON.forEach(function (v) {
        if (v.settings && v.settings.usage_data && v.settings.usage_data.data) {
          $.ajax({
            type: 'POST',
            cache: false,
            url: v.settings.usage_data.url,
            data: v.settings.usage_data.data
          });
        }
      });
    }
  });

})(jQuery, Drupal, drupalSettings, once);
